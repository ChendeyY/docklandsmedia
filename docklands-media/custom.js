
$('.btn-navbar').click(function(){

    $('#menuCollapse').slideToggle('slow');
});


$('.read-more-collapse').click(function(){
    $('#descriptionDocklands').slideToggle('slow');

    $(this).toggleClass('open');
});


$('.collapsed').click(function(){

	var elem = $(this).closest('.course-item'),
        button = $(this).closest('.collapsed-btn');

	elem.find('.course-item-visible').slideToggle('slow');
    elem.find('.course-item-hidden').slideToggle('slow');

    if (elem.find('.course-logo').hasClass("smoller")) {
    	elem.find('.course-logo').removeClass("smoller")
    }
    else {
    	elem.find('.course-logo').addClass("smoller")
    }

    button.toggleClass('toggle');
});


$('.side-bar-header').click(function(){ 
    $(this).parent().find('.side-bar-item-menu').slideToggle('slow');

    if($(this).hasClass('active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active')
    }

})

$('.side-bar-item').click(function(){
   $(this).find('.sub-side-bar').slideToggle('slow');
});

(function ($){
    $(".training-level").click(function(){
        
        $(this).toggleClass("expanded");
    });
})(jQuery);