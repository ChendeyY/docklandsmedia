<?php
/**
 * Template Name: error
 * Header template (header.php)
 * @package WordPress
 * @subpackage docklands
 */

get_header(); // include header.php ?> 

	<article class="main-content">
		<div class="container">
			<div class="page-not-exist">
				<img class="error-img" src="<?php echo get_template_directory_uri(); ?>/images/Error.png">
				<h1 class="page-not-exist-title">Oops! You are looking for a page that doesn't exist...</h1>
				<p class="page-not-exist-advice">Try using the top menu to start again.</p>
			</div>
		</div>
	</article>

<?php get_footer(); // include footer.php ?>