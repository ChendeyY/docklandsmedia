<?php
/**
 * Template Name: single-blog
 * @package WordPress
 * @subpackage docklands
 */
get_header(); // include header.php ?> 
	<article class="main-content">
		<div class="container">
			<div class="row main-block-wrap">
				<section class="col-sm-8 col-sm-push-4 content-wrapper">
					<h1 class="content-header single-blog">
						<?php echo $post->post_title; ?>
						<span class="news-date"><?php echo date('d/m/Y', strtotime($post->post_date)); ?></span>
					</h1>
					<div class="content main-content">
						<div class="news-item-wrapper"> 
							<?php echo $post->post_content; ?>
						</div>
						<div class="menu-wrapper">
							<button class="share-button"><i class="fa fa-link"></i></button>
							<?php echo do_shortcode('[ssba]'); ?>
						</div>
						<div id="url-for-share-wrap">
							<input class="share-link" id="share-link" type="text" />
						</div>
						<div class="post-pogination clearfix">
							<div class="allocated-title post-pogination-item previous">
								 <?php previous_post_link('%link', '<< Previous Post', TRUE); ?> 
							</div>
							<div class="allocated-title post-pogination-item next">
								<?php next_post_link('%link', 'Next Post >>', TRUE); ?>
							</div>
						</div>
						<?php comments_template( '', true ); ?>
					</div>
				</section>
			
				<section class="block-wrapper col-sm-pull-8 col-sm-4">
					<div class="blog-sidebar-header">
						<span class="blog-sidebar-header-icon"></span>
						BLOG
					</div>
					<div class="content left-side-bar">
						<?php get_search_form(true); ?>

						<?php
							$defaults = array(
								'theme_location'  => 'blog',
								'menu_class'      => 'blog-side-bar-menu',
								'menu_id'         => 'blog_sidebar_menu',
							);

							wp_nav_menu( $defaults );
						?>

					</div>
				</section>	
				
			</div>
		</div>
	</article>

<?php get_footer(); // include footer.php ?>