<?php
/*
Template Name: Search Form
*/
?>

<form role="search" method="get" id="searchform" class="searchform" action="/">
	<div class="input-group">
		<input type="text" class="form-control search" placeholder="Search for..." name="s" id="s">
		<span class="input-group-btn">
			<input  class="btn-search" type="submit" id="searchsubmit" value="">
		</span>
	</div>
</form>

