<?php
/**
 * Template Name: blog
 * Header template (header.php)
 * @package WordPress
 * @subpackage docklands
 */

get_header(); // include header.php ?> 

	<article class="main-content">
		<div class="container">
			<div class="row main-block-wrap">
			
				<section class="col-sm-8 col-sm-push-4 content-wrapper">
					<div class="content-header">
						Latest News from Docklands Media
					</div>
					<div class="content main-content">
						<div class="news-item-wrapper">
							<div class="title-of-news-wrapper">
								<span class="allocated-title title-of-news">Lorem ipsum dolor sit amet</span>
								<span class="date-of-news">12/09/2015</span>
							</div>
							<img src="images/news-1.png" class="news-images">
							<p class="description-news">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non. 
								<a href="">Read More</a>
							</p>
						</div>
						<div class="news-item-wrapper">
							<div class="title-of-news-wrapper">
								<span class="allocated-title title-of-news">Lorem ipsum dolor sit amet</span>
								<span class="date-of-news">12/09/2015</span>
							</div>
							<img src="images/news-2.png" class="news-images">
							<p class="description-news">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquipn. 
								<a href="">Read More</a>
							</p>
						</div>
						<div class="news-item-wrapper">
							<div class="title-of-news-wrapper">
								<span class="allocated-title title-of-news">Lorem ipsum dolor sit amet</span>
								<span class="date-of-news">12/09/2015</span>
							</div>
							<img src="images/news-3.png" class="news-images">
							<p class="description-news">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquipn. 
								<a href="">Read More</a>
							</p>
						</div>
						<div class="news-item-wrapper">
							<div class="title-of-news-wrapper">
								<span class="allocated-title title-of-news">Lorem ipsum dolor sit amet</span>
								<span class="date-of-news">12/09/2015</span>
							</div>
							<img src="images/news-4.png" class="news-images">
							<p class="description-news" class="news-images">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquipn. 
								<a href="">Read More</a>
							</p>
						</div>
						<div class="news-item-wrapper">
							<div class="title-of-news-wrapper">
								<span class="allocated-title title-of-news">Lorem ipsum dolor sit amet</span>
								<span class="date-of-news">12/09/2015</span>
							</div>
							<img src="images/news-5.png" class="news-images">
							<p class="description-news">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquipn. 
								<a href="">Read More</a>
							</p>
						</div>
						<div class="pagination-wrapper">
							<ul class="menu-pagination">
								<li><a href=""><<</a></li>
								<li class="activ"><a href="">1</a></li>
								<li><a href="">2</a></li>
								<li><a href="">3</a></li>
								<li><a href="">4</a></li>
								<li><a href="">5</a></li>
								<li><a href="">>></a></li>
							</ul>
						</div>
					</div>
				</section>

				<section class="block-wrapper col-sm-pull-8 col-sm-4">
					<div class="blog-sidebar-header">
						<span class="blog-sidebar-header-icon"></span>
						BLOG
					</div>
					<div class="content left-side-bar">
						<div class="input-group">
							<input type="text" class="form-control search" placeholder="Search for...">
							<span class="input-group-btn">
								<button class="btn-search" type="button"></button>
							</span>
						</div>

						<?php
							$defaults = array(
								'theme_location'  => 'blog',
								'menu_class'      => 'blog-side-bar-menu',
								'menu_id'         => 'blog_sidebar_menu',
							);

							wp_nav_menu( $defaults );
						?>

					</div>
				</section>		
			</div>
		</div>
	</article>

<?php get_footer(); // include footer.php ?>