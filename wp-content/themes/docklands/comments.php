<?php
/**
 * Template Name: comments.php
 * @package WordPress
 * @subpackage docklands
 */
?>
<?php 
$comments = get_comments();
$parentComments = array();
$childrenComments = array();
foreach ($comments as $comment) {
	if ($comment->comment_parent == '0') {
		$parentComments[$comment->comment_ID] = $comment;
		$parentComments[$comment->comment_ID]->children = array();
		continue;
	}

	$childrenComments[$comment->comment_ID] = $comment;
}

foreach ($childrenComments as $child) {
	if (empty($parentComments[$child->comment_parent])) {
		continue;
	}

	$parentComments[$child->comment_parent]->children[$child->comment_ID] = $child;
}

?> 

<?php comment_form(); ?>

<div class="comments-content">
	<div class="allocated-title comments" id="comment-form">Comments</div>
<!-- 	<form class="comment-form">
		<div class="form-group inline-block">
			<label for="exampleInputName2">Name:</label>
			<input type="text" class="form-control name-input" id="exampleInputName2">
		</div>
		<div class="form-group inline-block">
			<label for="exampleInputEmail2">Captcha:</label>
			<input type="text" class="form-control captcha-input" id="exampleInputEmail2">
			<span class="captcha-image"></span>
		</div>
		<div>
			<label>Comment:</label>
			<textarea class="form-control comment-area" row="3"></textarea>
		</div>
	</form> -->
	<?php if (!empty($parentComments)) :
		foreach ($parentComments as $key => $comment) : ?>
			<div class="commnet-wrapper clearfix">
				<div class="name-of-autor">
					<?php echo $comment->comment_author; ?>
					<span class="time-publish"><?php echo human_time_diff(strtotime($comment->comment_date)); ?> ago</span>
				</div>
				<p class="comment"><?php echo $comment->comment_content; ?></p>
				<a href="?replytocom=<?php echo $comment->comment_ID; ?>" class="reply comment-reply-link" rel="nofollow">reply</a>
				<?php if (!empty($comment->children)) : 
					foreach ($comment->children as $child) : ?>
						<div class="commnet-wrapper child clearfix">
							<div class="name-of-autor">
								<?php echo $child->comment_author; ?>
								<span class="time-publish"><?php echo human_time_diff(strtotime($child->comment_date)); ?> ago</span>
							</div>
							<p class="comment"><?php echo $child->comment_content; ?></p>
							<a href="?replytocom=<?php echo $comment->comment_ID; ?>" class="reply comment-reply-link" rel="nofollow">reply</a>
						</div>
					<?php endforeach;
				endif; ?>
			</div>
		<?php endforeach;
	endif;?>
</div>