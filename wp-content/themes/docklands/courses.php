<?php
/**
 * Template Name: courses
 * Header template (header.php)
 * @package WordPress
 * @subpackage docklands
 */

get_header(); // include header.php ?> 
	<article class="main-content">
		<div class="container">
			<div class="row main-block-wrap">
				<section class="col-sm-8 col-sm-push-4 content-wrapper">
					<div class="content-header">
						<?php the_field("title") ?>
					</div>				

					<div class="content main-content">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : 
							$imgURL = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
							echo '<img class="category-img" src="'.$imgURL.'">';
						endif; ?>

						<div class="show-on-mobile">
							<?php
								$defaults = array(
									'theme_location'  => 'courses',
									'container'       => 'div',
									'container_class' => 'sidebar-menu-wrapper',
									'menu_class'      => 'side-bar-menu courses-menu',
									'menu_id'         => 'courses_sidebar_menu',
								);
								wp_nav_menu( $defaults );
							?>
						</div>
						<div class="news-item-wrapper">
							<?php the_field("content") ?>
						</div>
					</div>

				</section>
				
				<section class="block-wrapper col-sm-pull-8 col-sm-4 hide-on-mobile">

					<?php
						$defaults = array(
							'theme_location'  => 'courses',
							'container'       => 'div',
							'container_class' => 'sidebar-menu-wrapper',
							'menu_class'      => 'side-bar-menu courses-menu',
							'menu_id'         => 'courses_sidebar_menu',
						);
						wp_nav_menu( $defaults );
					?>

				</section>

			</div>
		</div>
	</article>

<?php get_footer(); // include footer.php ?>