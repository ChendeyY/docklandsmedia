<?php
/**
 * Header template (header.php)
 * @package WordPress
 * @subpackage docklands
 */
?>

<!DOCTYPE html>
<html>
	<head>
<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php wp_head(); // necessary for work plugins and functionality wp ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73316349-1', 'auto');
  ga('send', 'pageview');

</script>

	</head>
	<body>
		<header>
			<div class="container">
				<div class="navbar-inner">
					<div class="logo-and-slogan-wrapper clearfix">
						<a href="/" class="brand"></a>
						<div class="slogan">
	                        <p class="first-line"><span class="selected-text">creative</span> media training</p>
	                        <p class="second-line">by industry professionals</p>
	                        <div class="share_buttons text-right">
	                            <a href="https://twitter.com/DocklandsMedia" class="twitter" target="_blank"></a>
	                            <a href="https://www.facebook.com/pages/Docklands-Media/166942016839578" class="facebook" target="_blank"></a>
	                            <a href="http://www.linkedin.com/company/docklands-media" class="linked-in" target="_blank"></a>
	                        </div>
	                    </div>
	                    
						<button class="btn-collapsed-navbar">
		                    <span class="icon-bar"></span>
		                </button>					
					</div>

					<?php
						$defaults = array(							
							'theme_location'  => 'navbar-menu',
							'container'       => 'nav',
							'container_class' => 'navbar navbar-customize',
							'container_id'    => 'menuCollapse',
							'menu_class'      => 'nav navbar-nav navbar-menu',
							'menu_id'         => 'navbar-menu',
						);
						wp_nav_menu( $defaults );
					?>

				</div>
			</div>
		</header>