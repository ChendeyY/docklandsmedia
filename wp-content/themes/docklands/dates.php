<?php
/**
 * Template Name: dates
 * @package WordPress
 * @subpackage docklands
 */
?>
<div class="title-block">
	<h3>Title:</h3>
	<input type="text" placeholder="Enter title here" value="<?php echo $title; ?>">	
</div>

<table class="dates-table dates-table-<?php echo $id; ?>" data-id="<?php echo $id; ?>">
	<thead>
		<tr>
			<td>id</td>
			<td>name</td>
			<td>start</td>
			<td>end</td>
			<td></td>
		</tr>
	</thead>
	<tbody>
		<?php 
			if (!empty($data)) :
			foreach ($data as $value) :
		?>
		<tr>
			<td class="col-1"><?php echo $value['id']; ?></td>
			<td class="col-2">
				<span class="input-dashed"><?php echo $value['name']; ?></span>
				<input class="edit-text"></input>
				<span class="dashicons dashicons-yes"></span>
			</td>
			<td class="col-3"><input type="text" class="datepicker" id="datepicker-<?php echo $value['id']; ?>-start" value="<?php echo $value['start']; ?>"></td>
			<td class="col-4"><input type="text" class="datepicker" id="datepicker-<?php echo $value['id']; ?>-end" value="<?php echo $value['end']; ?>"></td>
			<td class="col-5">
				<span class="dashicons dashicons-trash"></span>
				<span class="dashicons dashicons-admin-page"></span>
			</td>
		</tr>
		<?php 
			endforeach;
			endif;
		?>
	</tbody>
	<tfoot>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>
				<span class="add-row-date dashicons dashicons-plus-alt"></span>
			</td>
		</tr>
	</tfoot>	
</table>
<span class="save-button button button-primary button-large" data-id="<?php echo $id; ?>">Save</span>
<span class="error-msg"></span>