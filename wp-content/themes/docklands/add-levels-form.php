<div class="levels-form-content">
	<table>
		<thead>
			<tr>
				<td>Title</td>
				<td>Duration</td>
				<td></td>
			</tr>
		</thead>
		<tbody>
			<?php if (!empty($levels)) : ?>
			<?php foreach ($levels as $key => $level) : ?>
				<tr>
					<td class="editable"><?php echo $level->title; ?></td>
					<td class="editable"><?php echo $level->duration; ?></td>
					<td>
						<span class="dashicons dashicons-admin-customizer"></span>
						<span class="dashicons dashicons-trash"></span>
						<span class="dashicons dashicons-admin-page"></span>
						<span class="dashicons dashicons-plus"></span>
					</td>
				</tr>
			<?php endforeach; ?>
			
			<?php endif; ?>
		</tbody>
	</table>
</div>