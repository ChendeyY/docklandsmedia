<?php
/**
 * Template Name: subCourse-single
 * @package WordPress
 * @subpackage docklands
 */

get_header(); // include header.php 
$slides = getSlides($post->ID);

$fullwidth = '';
$padding0 = '';
$training_course = 'training-course';
if (empty($slides)) {
	$fullwidth = 'full-width';
	$training_course = 'padding0';
}

?>
	<article class="main-content single-course-template">
		<div class="container">
			<div class="row main-block-wrap">
				<section class="col-sm-8 col-sm-push-4 content-wrapper">
					<h1 class="content-header training-course">
						<?php echo get_field("title"); ?>
					</h1>
					<div class="content main-content">

						<div class='slider-wrap <?php echo $training_course; ?>'>
							<?php if (!empty($slides)) { ?>
								<div class="my-slider">
									<ul>
									<?php foreach ($slides as $key => $value) : ?>
										<li><img src="<?php echo $value; ?>" /></li>
									<?php endforeach; ?>
									</ul>
								</div>
							<?php } ?>
						</div>

						<div class="training-level-wrap">
						<?php $fields = get_fields();

						if (!empty($fields)) {
							foreach ($fields as $key => $field) {
								if (strpos($key, 'title_') !== 0) {
									continue;
								}
								$id = substr($key, 6);
								$contentKey = 'content_' . $id;
								if ($fields[$contentKey] === '' || $field === '') {
									continue;
								}

								echo '<div class="training-level">
										<h2 class="training-levels-title">
											' . $field . ' <span class="triangle-icon"></span>
										</h2>
										<div class="training-level-description">
											' . $fields[$contentKey] . '
										</div>						
									</div>';
							}
						} 
						?>
						</div>

						<div class="news-item-wrapper">
							<?php echo $post->post_content; ?>
						</div>
					</div>
				</section>
				
				<section class="block-wrapper col-sm-pull-8 col-sm-4">

					<?php
						$defaults = array(
							'theme_location'  => 'courses',
							'container'       => 'div',
							'container_class' => 'sidebar-menu-wrapper',
							'menu_class'      => 'side-bar-menu courses-menu',
							'menu_id'         => 'courses_sidebar_menu',
						);
						wp_nav_menu( $defaults );
					?>
				</section>
			</div>
		</div>
	</article>

<?php get_footer(); // include footer.php ?>
					