<?php

/**
 * Template Name: contact
 * Header template (header.php)
 * @package WordPress
 * @subpackage docklands
 */

get_header(); // include header.php ?> 

	<article class="main-content content-contuct">
		<div class="container padding-top10">
			<section class="contact-us-wrapper">
				<h2 class="contact-us-title">
					<?php the_field("title") ?>
				</h2>
				<div class="contact-us">
					<p class="message-thank"><b>Thanks!</b> Your email has been delivered. Huzzah!</p>
					<div class="contact-us-item first-item form-wrapper">
						<span class="contact-us-item-logo on-line-form"></span>
						<span class="contact-us-item-name">on-line form</span>
						<div class="row">
							<div class="col-sm-12 no-gutters">
								<?php echo do_shortcode('[contact-form-7 id="685" title="Contact form"]'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-5 padding-right-media">	
							<div class="contact-us-item-wrapper">
								<div class="contact-us-item">
									<span class="contact-us-item-logo e-mail"></span>
									<span class="contact-us-item-name">e-mail us</span>
								</div>
								<address>
									<?php the_field("e-mail") ?>
								</address>		
							</div>
							<div class="contact-us-item-wrapper">			
								<div class="contact-us-item">
									<span class="contact-us-item-logo coll-us"></span>
									<span class="contact-us-item-name">call us</span>
								</div>
								<address>
									<?php the_field("phone") ?>
								</address>							
							</div>
							<div class="contact-us-item-wrapper">
								<div class="contact-us-item">
									<span class="contact-us-item-logo fax-us"></span>
									<span class="contact-us-item-name">fax us</span>
								</div>
								<address>
									<?php the_field("fax") ?>
								</address>
							</div>
							<div class="contact-us-item-wrapper">				
								<div class="contact-us-item">
									<span class="contact-us-item-logo connect-with-us"></span>
									<span class="contact-us-item-name">connect with us</span>
								</div>
								<div class="social-buttons">
									<a class="facebook" href="https://www.facebook.com/pages/Docklands-Media/166942016839578" target="_blank"></a>
									<a class="twitter" href="https://twitter.com/DocklandsMedia" target="_blank"></a>
									<a class="linkedin" href="http://www.linkedin.com/company/docklands-media" target="_blank"></a>
									<a class="vimeo" href="https://vimeo.com/" target="_blank"></a>
									<a class="youtube" href="http://www.youtube.com/" target="_blank"></a>
									<a class="google" href="https://plus.google.com/+Docklandsmedia" target="_blank"></a>
								</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-7">
							<div class="contact-us-item-wrapper">
								<div class="contact-us-item">
									<span class="contact-us-item-logo find-us"></span>
									<span class="contact-us-item-name">find us</span>
								</div>
								<address>
									<?php the_field("address") ?>									
								</address>
							</div>
							<?php echo do_shortcode('[wpgmza id="1"]'); ?>					
						</div>
					</div>
				</div>
			</section>
		</div>
	</article>	

<?php get_footer(); // include footer.php ?>