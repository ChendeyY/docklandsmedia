<?php
/**
 * Template Name: footer
 * @package WordPress
 * @subpackage docklands
 */
?>
        <footer>
            <div class="container clearfix">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 bottom-right-side-wrapper">
                        <ul class="bottom-center clearfix">
                            <li class="bottom-right-side-item">
                                <a>
                                    <img src="/wp-content/themes/docklands/images/Adobe.png" alt="">
                                </a>
                            </li>
                            <li class="bottom-right-side-item">
                                <a>
                                    <img src="/wp-content/themes/docklands/images/maxon.png" alt="">
                                </a>
                            </li>
                            <li class="bottom-right-side-item">
                                <a>
                                    <img src="/wp-content/themes/docklands/images/Autodesk.png" alt="">
                                </a>
                            </li>
                            <li class="media-line"></li>
                            <li class="bottom-right-side-item">
                                <a>
                                    <img src="/wp-content/themes/docklands/images/realflow.png" alt="">
                                </a>
                            </li>
                            <li class="bottom-right-side-item">
                                <a>
                                    <img src="/wp-content/themes/docklands/images/Apple.png" alt="">
                                </a>
                            </li>
                            <li class="bottom-right-side-item">
                                <a>
                                    <img src="/wp-content/themes/docklands/images/Certiport.png" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 bottom-links-wrapper clearfix">
                        <?php
                            $defaults = array(
                                'theme_location'  => 'footer',
                                'container'       => 'div',
                                'container_class' => 'footer-menu-wrapper',
                                'menu_class'      => 'footer-menu',
                                'menu_id'         => 'footer_menu',
                            );
                            wp_nav_menu( $defaults );
                        ?>
                    </div>
                    <div class="col-sm-12 copy-right">
                        <p class="text-uppercase">docklands media &copy;<?php echo date("Y"); ?></p>
                    </div>
                </div>
            </div>
        </footer>
        <?php wp_footer(); // necessary for work plugins and functionality wp ?>
        <script>
var myVar = setInterval(function(){ googlead() }, 1000);
function googlead()
{
if(jQuery('.wpcf7-mail-sent-ok').length > 0)
{
var label = window.location.pathname.split('/')[1];
ga('send','event','form','submit',label);
clearInterval(myVar);
}
}
</script>
    </body>
</html>
