<?php
add_filter( 'wp_image_editors', 'change_graphic_lib' );

add_theme_support( 'post-thumbnails' ); 

function change_graphic_lib($array) {
  return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}
    add_action('wp_enqueue_scripts', 'enableScripts');

    /**
     * Add stylesheet to the page
     */
    function enableScripts()
    {
        wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css?v=1.4', array(), '1' );
        wp_enqueue_style( 'main-styles', get_template_directory_uri() . '/styles/main.css?v=1.33', array(), '1' );
        wp_enqueue_style( 'side-bar', get_template_directory_uri() . '/styles/left-side-bar.css?v=1.4', array(), '1' );
        wp_enqueue_style( 'blog', get_template_directory_uri() . '/styles/blog.css?v=1.6', array(), '1' );
        wp_enqueue_style( 'contact-us', get_template_directory_uri() . '/styles/contact-us.css?v=1.4', array(), '1' );
        wp_enqueue_style( 'about-us', get_template_directory_uri() . '/styles/about-us.css?v=1.4', array(), '1' );
        wp_enqueue_style( 'home-page', get_template_directory_uri() . '/styles/home-page.css?v=1.4', array(), '1' );
        wp_enqueue_style( 'courses', get_template_directory_uri() . '/styles/courses.css?v=1.6', array(), '1' );
        wp_enqueue_style( 'bundles', get_template_directory_uri() . '/styles/bundles.css?v=1.4', array(), '1' );
        wp_enqueue_style( 'mediaqueries', get_template_directory_uri() . '/styles/media-query.css?v=1.44', array(), '1' );
        wp_enqueue_style( 'unslider', get_template_directory_uri() . '/styles/unslider.css?v=1.4', array(), '1' );
        wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/bootstrap/bootstrap.min.css', array(), '1' );
        wp_enqueue_style( 'bxslider', get_template_directory_uri() . '/styles/jquery.bxslider.css', array(), '1' );
        wp_enqueue_script( 'bxslider', get_template_directory_uri() . '/js/jquery.bxslider.min.js', array( 'jquery' ), false );

        wp_enqueue_script( 'unslider', get_template_directory_uri() . '/js/unslider.js', array( 'jquery' ), false );
        wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/scripts/custom.js?v=1.4', array( 'jquery' , 'bxslider'), false );
    }


    add_action( 'after_setup_theme', 'registerCoursesMenu' );
    function registerCoursesMenu()
    {
      register_nav_menu( 'courses', __( 'Courses menu', 'theme-slug' ) );
      register_nav_menu( 'blog', __( 'Blog menu', 'theme-slug' ) );
      register_nav_menu( 'navbar-menu', __( 'Navbar menu', 'theme-slug' ) );
      register_nav_menu( 'about-us', __( 'About-us menu', 'theme-slug' ) );
      register_nav_menu( 'bundles', __( 'Bundles menu', 'theme-slug' ) );
      register_nav_menu( 'footer', __( 'Footer menu', 'theme-slug' ) );
    }

    add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
    function special_nav_class($classes, $item)
    {
         if( in_array('blog-side-bar-menu', $classes) ){
                 $classes[] = 'active ';
         }
         return $classes;
    }

    function remove_comment_fields($fields)
    {
        unset($fields['url']);
        
        return $fields;
    }
    add_filter('comment_form_default_fields', 'remove_comment_fields');

    function addDatesMetaBox()
    {
        $screens = array('post', 'course', 'bundle');

        foreach ($screens as $screen) {
            add_meta_box(
                'event-dates-1', 
                'Dates-1', 
                'getMetaBoxTemplate1', 
                $screen, 
                'normal',
                'high'
            );
            add_meta_box(
                'event-dates-2', 
                'Dates-2', 
                'getMetaBoxTemplate2', 
                $screen, 
                'normal',
                'high'
            );
            add_meta_box(
                'event-dates-3', 
                'Dates-3', 
                'getMetaBoxTemplate3', 
                $screen, 
                'normal',
                'high'
            );
        }
    }
    add_action( 'add_meta_boxes', 'addDatesMetaBox');

    function getMetaBoxTemplate1($post)
    {
        global $add_my_script;
        $add_my_script = true;

        $data = unserialize(get_post_meta($post->ID, 'events-1', true));
        $title = get_post_meta($post->ID, 'events-title-1', true);
        if (!$data) {
            $data = array();
        }
        $id = 1;

        include(locate_template('dates.php'));
    }

    function getMetaBoxTemplate2($post, $id)
    {
        global $add_my_script;
        $add_my_script = true;

        $data = unserialize(get_post_meta($post->ID, 'events-2', true));
        $title = get_post_meta($post->ID, 'events-title-2', true);

        if (!$data) {
            $data = array();
        }
        $id = 2;

        include(locate_template('dates.php'));
    }

    function getMetaBoxTemplate3($post, $id)
    {
        global $add_my_script;
        $add_my_script = true;

        $data = unserialize(get_post_meta($post->ID, 'events-3', true));
        $title = get_post_meta($post->ID, 'events-title-3', true);

        if (!$data) {
            $data = array();
        }
        $id = 3;

        include(locate_template('dates.php'));
    }

    add_action('init', 'registerJqueryDatePicker');
    function registerJqueryDatePicker()
    {
        wp_register_script(
            'jquery-ui',
            get_template_directory_uri() . '/js/admin/jquery-ui.js',
            array('jquery'),
            '1.0',
            true
        );
        wp_register_script(
            'dates',
            get_template_directory_uri() . '/js/admin/dates.js',
            array('jquery', 'jquery-ui'),
            '1.0',
            true
        );
    }

    add_action('admin_footer', 'printMyScript');
    function printMyScript()
    {
        global $add_my_script;

        if ( ! $add_my_script )
            return;

        wp_enqueue_script('jquery-ui');
        wp_enqueue_script('dates');
        $translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
        wp_localize_script( 'dates', 'params', $translation_array );
    }

    add_action('admin_enqueue_scripts', 'printMyCSS');
    function printMyCSS()
    {
        wp_enqueue_style( 'jquery-ui', get_template_directory_uri() . '/styles/admin/jquery-ui.css', array(), '1');
        
        wp_enqueue_style( 'custom-admin', get_template_directory_uri() . '/styles/admin/style.css', array(), '1' );
    }

    add_action( 'wp_ajax_save_events', 'saveEvents' );

    function saveEvents()
    {
        $url = wp_get_referer();
        $params = array();
        parse_str(parse_url($url)['query'], $params);
        $postId = $params['post'];
        if (!isset($_POST['table'])) {
            $table = serialize(array());
        } else {
            $table = serialize($_POST['table']);
        }
        $id = $_POST['id'];
        $title = (!isset($_POST['title'])) ? '' : $_POST['title'];
        update_post_meta($postId, 'events-title-'.$id, $title);
        update_post_meta($postId, 'events-'.$id, $table);

        die(json_encode(array('status' => true)));
    }

    function sortByDate(&$events)
    {
        if (empty($events)) {
            return false;
        }
        usort($events, function($a, $b) {
           return strcmp(strtotime($a['start']), strtotime($b['start']));
        });
    }

    function getSlides($postId)
    {
        global $wpdb;
        $slides = array();

        $query = "SELECT * FROM " . $wpdb->prefix . "postmeta WHERE post_id=" . $postId . " AND meta_key LIKE 'slide%'";

        $results = $wpdb->get_results($query);

        if (empty($results) || !$results) {
            return $slides;
        }

        foreach ($results as $result) {
            $tmp = (int)substr($result->meta_key, 5);
            if ($tmp == 0) continue;

            $src = wp_get_attachment_image_src($result->meta_value, 'full');
            if ($src === false) continue;

            $slides[$tmp] = $src[0];
        }

        return $slides;
    }

    function add_custom_classes($classes, $item){
        global $post;
        if (is_single()) {
            if ($item->title == 'courses' && $post->post_type == 'course') {
                $classes[] = 'current-menu-item';
            }
            if ($item->title == 'bundles' && $post->post_type == 'bundle') {
                $classes[] = 'current-menu-item';
            }
            if ($item->title == 'blog' && $post->post_type == 'post') {
                $classes[] = 'current-menu-item';
            }
        }
        return $classes;
    }

    add_filter('nav_menu_css_class' , 'add_custom_classes' , 10 , 2);

?>
