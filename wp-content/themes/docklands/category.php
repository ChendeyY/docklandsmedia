<?php
/**
 * Template Name: category
 * @package WordPress
 * @subpackage docklands
 */

get_header(); // include header.php ?> 


<?php 

	require_once get_template_directory() . '/class.PaginationLinks.php';
	$category = get_category($cat);
	$limit = 3;
	$count = ceil($category->category_count / $limit);
	$offset = 0;
	$curPage = 1;
	if (isset($_GET['pagenum'])) {
		$offset = $limit * (int)$_GET['pagenum'] - $limit;
		$curPage = (int)$_GET['pagenum'];
	}

	$prev = $curPage - 1;
	$next = $curPage + 1;

?>

	<article class="main-content">
		<div class="container">
			<div class="row main-block-wrap">			
				<section class="col-sm-8 col-sm-push-4 content-wrapper">
					<h1 class="content-header category-subtitle">
						<?php echo $category->name ?>
					</h1>
					<div class="content main-content">

						<?php 
							$args = array ( 'category' => $category->cat_ID, 'posts_per_page' => $limit, 'offset' => $offset);
							$posts = get_posts( $args );
							foreach( $posts as $post ) : setup_postdata($post);
							
						?>
							<div class="news-item-wrapper">
								<h2 class="title-of-news-wrapper">
									<a href="<?php echo get_permalink($post->ID);?>"
									   class="allocated-title title-of-news"><?php echo $post->post_title; ?>
								    </a>
									<span class="date-of-news"><?php echo date('d/m/Y', strtotime($post->post_date)); ?></span>
								</h2>
								<?php the_post_thumbnail(); ?>
								<div class="category-post-content description-news">
									<?php the_excerpt(); ?>
									<a href="<?php echo get_permalink($post->ID);?>">Read More</a>
								</div>
							</div>
						<?php endforeach; ?>
						
						<div class="pagination-wrapper">
							<ul class="menu-pagination">
								<?php if ($curPage > 1) : ?><li><a href="?pagenum=<?php echo $prev; ?>">&lt;&lt;</a></li> <?php endif; ?>
								<?php echo PaginationLinks::create(
								    $curPage,
								    $count,
								    1,
								    '<li><a href="?pagenum=%d">%d</a></li>',
								    '<li class="activ"><a href="?pagenum=%d">%d</a></li>',
								    '');
								?>
								<?php if ($curPage < $count) : ?><li><a href="?pagenum=<?php echo $next; ?>">&gt;&gt;</a></li> <?php endif; ?>
							</ul>
						</div>
					</div>
				</section>

				<section class="block-wrapper col-sm-pull-8 col-sm-4">
					<div class="blog-sidebar-header">
						<span class="blog-sidebar-header-icon"></span>
						BLOG
					</div>
					<div class="content left-side-bar">
						<?php get_search_form(true); ?>
						<?php
							$defaults = array(
								'theme_location'  => 'blog',
								'menu_class'      => 'blog-side-bar-menu',
								'menu_id'         => 'blog_sidebar_menu',
							);

							wp_nav_menu( $defaults );
						?>

					</div>
				</section>		
			</div>
		</div>
	</article>

<?php get_footer(); // include footer.php ?>