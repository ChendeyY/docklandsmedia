<?php
/*
Template Name: Search Page
*/
?>

<?php
global $query_string, $wp_query;
$query_string .= '&post_type=post&posts_per_page=1000';
$query_args = explode("&", $query_string);
$search_query = array();

foreach($query_args as $key => $string) {
	$query_split = explode("=", $string);
	$search_query[$query_split[0]] = urldecode($query_split[1]);
} // foreach

$search = new WP_Query($search_query);

$total = $wp_query->found_posts;

get_header();

$posts = $search->posts;
?>

<article class="main-content">
	<div class="container">
		<div class="row main-block-wrap">			
			<section class="col-sm-8 col-sm-push-4 content-wrapper">
				<div class="content-header">
					Search results
				</div>
				<div class="content main-content">
					<?php if (!empty($posts)) : foreach( $posts as $post ) : setup_postdata($post); ?>
							<div class="news-item-wrapper">
								<div class="title-of-news-wrapper">
									<a href="<?php echo get_permalink($post->ID);?>" 
									   class="allocated-title title-of-news"><?php echo $post->post_title; ?>
								    </a>
									<span class="date-of-news"><?php echo date('d/m/Y', strtotime($post->post_date)); ?></span>
								</div>
								<div class="category-post-content description-news">
									<?php the_excerpt(); ?>
									<a href="<?php echo get_permalink($post->ID);?>">Read More</a>
								</div>
							</div>
							
						<?php endforeach; ?>
						
						<?php else : ?>
						<h4>Sorry there are no results..</h4>
					<?php endif; ?>
					</div>
			</section>

			<section class="block-wrapper col-sm-pull-8 col-sm-4">
				<div class="blog-sidebar-header">
					<span class="blog-sidebar-header-icon"></span>
					BLOG
				</div>
				<div class="content left-side-bar">
					<?php get_search_form(true); 

					$defaults = array(
						'theme_location'  => 'blog',
						'menu_class'      => 'blog-side-bar-menu',
						'menu_id'         => 'blog_sidebar_menu',
					);

					wp_nav_menu( $defaults );
					?>

				</div>
			</section>		
		</div>
	</div>
</article>

<?php get_footer(); // include footer.php ?>
