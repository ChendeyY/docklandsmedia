<?php
/**
 * Template Name: front-page
 * @package WordPress
 * @subpackage docklands
 */

get_header(); // include header.php ?> 

	<article class="main-content">
		<div class="container padding-top10">
			<?php echo do_shortcode('[rev_slider alias="main-page"]'); ?>

			<div class="docklands-media-description hidden">
				<?php the_field("docklands_information") ?>
				<div class="block-collapse" id="descriptionDocklands">
					<?php the_field("docklands_information_read_more") ?>
				</div>
				<div class="text-right">
					<button class="read-more-collapse">
						<span class="read-more">Read More...</span>
						<span class="read-less">Read Less...</span>
					</button>
				</div>
			</div>

		    <section class="row">
	    		<div class="courses-wrapper">
	    			<div class="col-sm-6 col-md-4">
	    				<div class="course-item clearfix">
		    				<span class="course-logo" style="background-image: url(<?php echo get_field('main_icon_1')['url']; ?>)"></span>
		    				<div class="course-item-visible">
			    				<h3 class="course-name"><?php the_field("title_1") ?></h3>
			    				<div class="course-content-icons">
			    					<a class="course-icon" href="<?php the_field("link_1_1") ?>"><img src="<?php echo get_field('image_1_1')['url']; ?>"></a>
			    					<a class="course-icon" href="<?php the_field("link_1_2") ?>"><img src="<?php echo get_field('image_1_2')['url']; ?>"></a>
			    					<a class="course-icon" href="<?php the_field("link_1_3") ?>"><img src="<?php echo get_field('image_1_3')['url']; ?>"></a>
			    				</div>
		    				</div>		    				
		    				<div class="course-item-hidden">
		    					<p><?php the_field("more_1") ?></p>
		    				</div>
		    				<div class="collapsed-btn text-right">
		    					<span class="collapsed collaps-more">more...</span>
		    					<span class="collapsed collaps-less">less...</span>
		    				</div>				    					
	    				</div>
	    			</div>
	    			<div class="col-sm-6 col-md-4">
	    				<div class="course-item clearfix">
	    					<span class="course-logo" style="background-image: url(<?php echo get_field('main_icon_2')['url']; ?>)"></span>
		    				<div class="course-item-visible">
			    				<h3 class="course-name"><?php the_field("title_2") ?></h3>
			    				<div class="course-content-icons">
			    					<a class="course-icon" href="<?php the_field("link_2_1") ?>"><img src="<?php echo get_field('image_2_1')['url']; ?>"></a>
			    					<a class="course-icon" href="<?php the_field("link_2_2") ?>"><img src="<?php echo get_field('image_2_2')['url']; ?>"></a>
			    					<a class="course-icon" href="<?php the_field("link_2_3") ?>"><img src="<?php echo get_field('image_2_3')['url']; ?>"></a>
		    					</div>
		    				</div>
		    				<div class="course-item-hidden">
		    					<p><?php the_field("more_2") ?></p>
		    				</div>
		    				<div class="collapsed-btn text-right">
		    					<span class="collapsed collaps-more">more...</span>
		    					<span class="collapsed collaps-less">less...</span>
		    				</div>				    					
	    				</div>
	    			</div>
	    			<div class="col-sm-6 col-md-4">
	    				<div class="course-item clearfix">
		    				<span class="course-logo" style="background-image: url(<?php echo get_field('main_icon_3')['url']; ?>)"></span>
		    				<div class="course-item-visible">
			    				<h3 class="course-name"><?php the_field("title_3") ?></h3>
			    				<div class="course-content-icons">
			    					<a class="course-icon" href="<?php the_field("link_3_1") ?>"><img src="<?php echo get_field('image_3_1')['url']; ?>"></a>
			    					<a class="course-icon" href="<?php the_field("link_3_2") ?>"><img src="<?php echo get_field('image_3_2')['url']; ?>"></a>
			    					<a class="course-icon" href="<?php the_field("link_3_3") ?>"><img src="<?php echo get_field('image_3_3')['url']; ?>"></a>
		    					</div>
		    				</div>
		    				<div class="course-item-hidden">
		    					<p><?php the_field("more_3") ?></p>
		    				</div>
		    				<div class="collapsed-btn text-right">
		    					<span class="collapsed collaps-more">more...</span>
		    					<span class="collapsed collaps-less">less...</span>
		    				</div>				    					
	    				</div>
	    			</div>
	    			<div class="col-sm-6 col-md-4">
	    				<div class="course-item clearfix">
		    				<span class="course-logo" style="background-image: url(<?php echo get_field('main_icon_4')['url']; ?>)"></span>
		    				<div class="course-item-visible">
			    				<h3 class="course-name"><?php the_field("title_4") ?></h3>
			    				<div class="course-content-icons">
			    					<a class="course-icon" href="<?php the_field("link_4_1") ?>"><img src="<?php echo get_field('image_4_1')['url']; ?>"></a>
			    					<a class="course-icon" href="<?php the_field("link_4_2") ?>"><img src="<?php echo get_field('image_4_2')['url']; ?>"></a>
			    					<a class="course-icon" href="<?php the_field("link_4_3") ?>"><img src="<?php echo get_field('image_4_3')['url']; ?>"></a>
		    					</div>
		    				</div>
		    				<div class="course-item-hidden">
		    					<p><?php the_field("more_4") ?></p>
		    				</div>
		    				<div class="collapsed-btn text-right">
		    					<span class="collapsed collaps-more">more...</span>
		    					<span class="collapsed collaps-less">less...</span>
		    				</div>				    					
	    				</div>
	    			</div>
	    			<div class="col-sm-6 col-md-4">
	    				<div class="course-item clearfix">
		    				<span class="course-logo" style="background-image: url(<?php echo get_field('main_icon_5')['url']; ?>)"></span>
		    				<div class="course-item-visible">
			    				<h3 class="course-name"><?php the_field("title_5") ?></h3>
			    				<div class="course-content-icons">
			    					<a class="course-icon" href="<?php the_field("link_5_1") ?>"><img src="<?php echo get_field('image_5_1')['url']; ?>"></a>
			    					<a class="course-icon" href="<?php the_field("link_5_2") ?>"><img src="<?php echo get_field('image_5_2')['url']; ?>"></a>
			    					<a class="course-icon" href="<?php the_field("link_5_3") ?>"><img src="<?php echo get_field('image_5_3')['url']; ?>"></a>
		    					</div>
		    				</div>
		    				<div class="course-item-hidden">
		    					<p><?php the_field("more_5") ?></p>
		    				</div>
		    				<div class="collapsed-btn text-right">
		    					<span class="collapsed collaps-more">more...</span>
		    					<span class="collapsed collaps-less">less...</span>
		    				</div>				    					
	    				</div>
	    			</div>
	    			<div class="col-sm-6 col-md-4">
	    				<div class="course-item clearfix">
		    				<span class="course-logo" style="background-image: url(<?php echo get_field('main_icon_6')['url']; ?>)"></span>
		    				<div class="course-item-visible">
			    				<h3 class="course-name"><?php the_field("title_6") ?></h3>
			    				<div class="course-content-icons">
			    					<a class="course-icon" href="<?php the_field("link_6_1") ?>"><img src="<?php echo get_field('image_6_1')['url']; ?>"></a>
			    					<a class="course-icon" href="<?php the_field("link_6_2") ?>"><img src="<?php echo get_field('image_6_2')['url']; ?>"></a>
			    					<a class="course-icon" href="<?php the_field("link_6_3") ?>"><img src="<?php echo get_field('image_6_3')['url']; ?>"></a>
		    					</div>
		    				</div>
		    				<div class="course-item-hidden">
		    					<p><?php the_field("more_6") ?></p>
		    				</div>
		    				<div class="collapsed-btn text-right">
		    					<span class="collapsed collaps-more">more...</span>
		    					<span class="collapsed collaps-less">less...</span>
		    				</div>				    					
	    				</div>
	    			</div>
	    		</div>
		    </section>				    

			<div class="docklands-media-description">
				<div><?php the_field("docklands_information") ?></div>
				<div class="block-collapse">
					<div><?php the_field("docklands_information_read_more") ?></div>
				</div>
				<div class="text-right">
					<button class="read-more-collapse">
						<span class="read-more">Read More...</span>
						<span class="read-less">Read Less...</span>
					</button>
				</div>
			</div>
		</div>
	</article>

<?php get_footer(); // include footer.php ?>