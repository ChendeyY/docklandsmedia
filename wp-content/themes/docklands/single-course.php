<?php

/**

 * Template Name: single-course

 * @package WordPress

 * @subpackage docklands

 */



get_header(); // include header.php 

$events1 = unserialize(get_post_meta($post->ID, 'events-1', true)); 

$events2 = unserialize(get_post_meta($post->ID, 'events-2', true)); 

$events3 = unserialize(get_post_meta($post->ID, 'events-3', true)); 

$title1 = get_post_meta($post->ID, 'events-title-1', true);

$title2 = get_post_meta($post->ID, 'events-title-2', true);

$title3 = get_post_meta($post->ID, 'events-title-3', true);

sortByDate($events1);

sortByDate($events2);

sortByDate($events3);

$slides = getSlides($post->ID);

$courseImage = get_field('course_image');

$checkCourseImage = false;

if (isset($courseImage['url']) && !empty($courseImage['url']) && $courseImage['url'] !== false) {

	$checkCourseImage = true;

}

$fullwidth = '';

if ($post->post_content == '' && empty($slides)) {

	$fullwidth = 'full-width';

}

?>

	<article class="main-content single-course-template">

		<div class="container">

			<div class="row main-block-wrap">

				<section class="col-sm-8 col-sm-push-4 content-wrapper">

					<div class="course-entry-title">

						<?php if ($checkCourseImage) : ?>

						<img class="course-image" src="<?php echo $courseImage['url'];?>" alt="<?php echo $courseImage['alt']; ?>">

					<?php endif; ?>

						<h1 class="course-main-title"><?php echo get_field("course_full_title"); ?></h1>

					</div>

					<div class="content main-content cours-content">

						<div class='slider-wrap'>
							<?php if (!empty($slides)) { ?>
								<div class="my-slider">
									<ul>
									<?php foreach ($slides as $key => $value) : ?>
										<li><img src="<?php echo $value; ?>" /></li>
									<?php endforeach; ?>
									</ul>
								</div>
							<?php } ?>

						</div>

						<div class="post-content"><?php echo $post->post_content; ?></div>

						<div class="news-item-wrapper">

							<div class="course-info <?php echo $fullwidth; ?>">

								<h2 class="course-info-item">Level: <?php echo get_field("level"); ?></h2>

								<h2 class="course-info-item">Duration: <?php echo get_field("duration"); ?></h2>

								<h2 class="course-info-item separator">Cost <span>(ex VAT)</span></h2>

								<h2 class="course-info-item">Company: &pound;<?php echo get_field("company"); ?></h2>

								<h2 class="course-info-item">Self-funding: &pound;<?php echo get_field("self_funding"); ?></h2>

								<h2 class="course-info-item">Online: &pound;<?php echo get_field("online"); ?></h2>

							</div>

							<ul class="course-nav-bar">

								<li class="course-nav-bar-item active" data-target="overview">
									<h2>Overview</h2>
								</li>

								<li class="course-nav-bar-item" data-target="outline">
									<h2>Outline</h2>
								</li>

								<li class="course-nav-bar-item" data-target="dates">
									<h2>Dates</h2>
								</li>

								<li class="course-nav-bar-item" data-target="offers">
									<h2>Offers</h2>
								</li>

								<li class="course-nav-bar-item" data-target="enquiry">
									<h2>Book / Enquire</h2>
								</li>

							</ul>

							<div class="course-content-item course-content-overview" data-id-target="overview" id="overview">

								<?php echo get_field("overview"); ?>

							</div>

							<div class="course-content-item course-content-outline" data-id-target="outline" id="outline">

								<div class="row">

									<div><?php echo get_field("outline_main_text"); ?></div>

									<?php if ($post->ID == 314) { ?>

										<div><?php echo get_field("outline_1"); ?></div>

										<div><?php echo get_field("outline_2"); ?></div>

									<?php } else { ?>

									<div class="col-md-5 col-left"><?php echo get_field("outline_1"); ?></div>

									<div class="col-md-5 col-right"><?php echo get_field("outline_2"); ?></div>

									<?php }?>

								</div>

							</div>

							<div class="course-content-item course-content-dates" data-id-target="dates" id="dates">

								<?php echo get_field("dates_before"); ?>

								<div id="calendar">



									<?php if (!empty($events1)) : ?>

										<div class="col-md-3 date-col">

											<span class="dates-heading"><?php echo $title1; ?></span>

											<ul>

										<?php foreach ($events1 as $event) : ?>

												<li><span><?php echo (empty($event['name'])) ? date('d F Y', strtotime($event['start'])) : $event['name'] . ' - ' . date('d F Y', strtotime($event['start'])); ?></span></li>

										<?php endforeach; ?>

											</ul>

										</div>

									<?endif;?>



									<?php if (!empty($events2)) : ?>

										<div class="col-md-3 date-col">

											<span class="dates-heading"><?php echo $title2; ?></span>

											<ul>

										<?php foreach ($events2 as $event) : ?>

												<li><span><?php echo (empty($event['name'])) ? date('d F Y', strtotime($event['start'])) : $event['name'] . ' - ' . date('d F Y', strtotime($event['start'])); ?></span></li>

										<?php endforeach; ?>

											</ul>

										</div>

									<?endif;?>



									<?php if (!empty($events3)) : ?>

										<div class="col-md-3 date-col">

											<span class="dates-heading"><?php echo $title3; ?></span>

											<ul>

										<?php foreach ($events3 as $event) : ?>

												<li><span><?php echo (empty($event['name'])) ? date('d F Y', strtotime($event['start'])) : $event['name'] . ' - ' . date('d F Y', strtotime($event['start'])); ?></span></li>

										<?php endforeach; ?>

											</ul>

										</div>

									<?endif;?>

								</div>

								<?php echo get_field("dates_after"); ?>

							</div>

							<div class="course-content-item course-content-offers" data-id-target="offers" id="offers">

								<h3 class="offer-subtitle group-bookings">Group Bookings</h3>
								<?php echo get_field("group_bookings"); ?>

								<h3 class="offer-subtitle education">Education and Non-profit Organisations</h3>
								<?php echo get_field("education_and_non-profit_organisations"); ?>

								<h3 class="offer-subtitle training-credits">Training Credits</h3>
								<?php echo get_field("training_credits"); ?>

								<h3 class="offer-subtitle early-bird">Early Bird</h3>	
								<?php echo get_field("early_bird"); ?>

								<h3 class="offer-subtitle astudents">Students and Freelancers</h3>	
								<?php echo get_field("students_and_freelancers"); ?>

							</div>

							<div class="course-content-item course-content-enquiry" data-id-target="enquiry" id="enquiry">								

								<div class="enquiry-form">

									<div class="no-gutters">
										<p class="equire-description">
											Use the form below to make an enquiry about this course or to make a booking. One of our consultants will respond to your enquiry immediately during usual business hours.
										</p>

										<?php echo do_shortcode('[contact-form-7 id="849" title="Contact form_enquiry"]'); ?>

									</div>

								</div>

							</div>

						</div>

					</div>

				</section>

				

				<section class="block-wrapper col-sm-pull-8 col-sm-4">



					<?php

						$defaults = array(

							'theme_location'  => 'courses',

							'container'       => 'div',

							'container_class' => 'sidebar-menu-wrapper',

							'menu_class'      => 'side-bar-menu courses-menu',

							'menu_id'         => 'courses_sidebar_menu',

						);

						wp_nav_menu( $defaults );

					?>



				</section>



			</div>

		</div>

	</article>



<?php get_footer(); // include footer.php ?>