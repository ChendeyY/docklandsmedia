(function ($){

    $(document).ready(function(){

        $(".slider-wrap").css("display", "block");

        $('.my-slider').unslider({
            autoplay: true,
            infinite: true
        });

        $('.btn-collapsed-navbar').on('click', function(){
            $('#menuCollapse').slideToggle('slow');
        });

        $('.read-more-collapse').on('click', function(){
            $(this).parents('.docklands-media-description').find('.block-collapse').slideToggle('slow');
            $(this).toggleClass('open');
        });

        $('.collapsed').on('click', function(){
            var elem = $(this).closest('.course-item'),
                button = $(this).closest('.collapsed-btn');

            elem.find('.course-item-visible').slideToggle('slow');
            elem.find('.course-item-hidden').slideToggle('slow');

            if (elem.find('.course-logo').hasClass("smoller")) {
                elem.find('.course-logo').removeClass("smoller")
            }
            else {
                elem.find('.course-logo').addClass("smoller")
            }

            button.toggleClass('toggle');
        });   

        $('.side-bar-item').on('click', function(){
           $(this).find('.sub-side-bar').slideToggle('slow');
        });

        $(".training-levels-title").on('click', function(){
            $(this).parent().toggleClass("expanded");
            $(this).parent().find('.training-level-description').slideToggle('slow');
        });


        // -------------------- left-sidebar----------------------------

        $('.sidebar-menu-wrapper > ul > li > a').on('click', function() {
            if( $(this).parent().find('>ul').css('display') == 'block' ) {
                $(this).parent().find('>ul').slideUp();
                $(this).removeClass('active');
            } else {
                $('.sidebar-menu-wrapper > ul > li > ul').slideUp();
                $('.sidebar-menu-wrapper > ul > li > ul').slideUp();
                $('.sidebar-menu-wrapper > ul > li > a').removeClass('active');
                $(this).addClass('active');
                $(this).parent().find('>ul').slideDown();
            }
        }); 

        $('.sidebar-menu-wrapper > ul > li > ul > li > a').on('click', function() {
            if( $(this).parent().find('>ul').css('display') == 'block' ) {
                $(this).parent().find('>ul').slideUp();
                $(this).removeClass('active');
            } else {
                $('#courses_sidebar_menu > li > ul > li > ul').slideUp();
                $('#courses_sidebar_menu > li > ul>  li > a').removeClass('active');
                $(this).addClass('active');
                $(this).parent().find('>ul').slideDown();
            }
        });    

        // ------------------- Course tabs ------------------------
        
        var hash = location.hash.split('#')[1];

        if($('.course-content-item[data-id-target='+hash+']').length) {
            $('.course-nav-bar').find('.course-nav-bar-item').removeClass('active');
            $('.course-nav-bar > li[data-target='+hash+']').addClass('active');
            $('.course-content-item').css( "display", "none" );
            $('.course-content-item[data-id-target='+hash+']').css( "display", "block" );

            setTimeout(function(){
                $('html, body').animate({
                    scrollTop:  $('.course-nav-bar > li[data-target='+hash+']').offset().top
                }, 500);
            }, 500);
        }

        $('.course-nav-bar-item').on('click', function() {
            var attr = $(this).attr( "data-target");
            if ($(this).hasClass('active')) {
                return;
            }
            
            location.hash = attr;
            $(this).parent().find('.course-nav-bar-item').removeClass('active');
            $(this).addClass('active');
            $('.course-content-item').css( "display", "none" );
            $('.course-content-item[data-id-target='+attr+']').css( "display", "block" );
        })     

        // ------------------- Share button ------------------------

        $('.share-button').on('click', function() {
            $('#url-for-share-wrap').slideToggle();
            $('#share-link').val( window.location.href );
            $('#share-link').focus().select();
        });

        if ($('.single-course-template').length !== 0 || $('.home-page-slide').length !==0) {
            $('.bxslider').bxSlider({
              auto: true
            });
        }

    });

})(jQuery);