<?php
/**
 * Template Name: privacy-policy
 * @package WordPress
 * @subpackage docklands
 */

get_header(); // include header.php ?> 

	<article class="main-content">
		<div class="container">
			<div class="row main-block-wrap">
				
				<section class="col-sm-8 col-sm-push-4 content-wrapper">
					<div class="content-header">
						Privacy Policy
					</div>
					<div class="content main-content">
						<div class="news-item-wrapper">
							<?php echo $post->post_content; ?>
						</div>
					</div>
				</section>

				<section class="block-wrapper col-sm-pull-8 col-sm-4">

					<?php
						$defaults = array(
							'theme_location'  => 'about-us',
							'container'       => 'div',
							'container_class' => 'sidebar-menu-wrapper',
							'menu_class'      => 'side-bar-menu about-us',
							'menu_id'         => 'about_us_sidebar_menu',
						);
						wp_nav_menu( $defaults );
					?>
				</section>
			</div>
		</div>
	</article>

<?php get_footer(); // include footer.php ?>