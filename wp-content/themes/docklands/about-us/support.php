<?php
/**
 * Template Name: support
 * @package WordPress
 * @subpackage docklands
 */

get_header(); // include header.php ?> 

	<article class="main-content">
		<div class="container">
			<div class="row main-block-wrap">
				
				<section class="col-sm-8 col-sm-push-4 content-wrapper">
					<div class="content-header">
						<?php echo get_field("title_content"); ?>
					</div>
					<div class="content main-content">
						<div class="news-item-wrapper">

							<div class="support-content-title">
								<span class="support-content-icon"></span>
								docklands media promise
							</div>
							<?php echo get_field("docklands_media_promise"); ?>

							<div class="support-content-title">
								<span class="support-content-icon letter-icon"></span>
								e-mail support
							</div>
							<?php echo get_field("e-mail_support"); ?>

							<div class="support-content-title">
								<span class="support-content-icon phone-icon"></span>
								phone support
							</div>
							<?php echo get_field("phone_support"); ?>

						</div>
					</div>
				</section>

				<section class="block-wrapper col-sm-pull-8 col-sm-4">

					<?php
						$defaults = array(
							'theme_location'  => 'about-us',
							'container'       => 'div',
							'container_class' => 'sidebar-menu-wrapper',
							'menu_class'      => 'side-bar-menu about-us',
							'menu_id'         => 'about_us_sidebar_menu',
						);
						wp_nav_menu( $defaults );
					?>
				</section>
			</div>
		</div>
	</article>

<?php get_footer(); // include footer.php ?>