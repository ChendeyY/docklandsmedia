<?php

/**
 * Template Name: clients
 * @package WordPress
 * @subpackage docklands
 */



get_header(); // include header.php

$postFields = get_fields($post->ID);
$logos = array();

$logoKey = "logo";

foreach($postFields as $key => $value) {

	$keyTemplate = $logoKey;
	$shards = explode($keyTemplate, $key);

	if (2 === count($shards)) {
		$logos[$shards[1]][$keyTemplate] = $value;
	}
};

?> 

	<article class="main-content">
		<div class="container">
			<div class="row main-block-wrap">
				<section class="col-sm-8 col-sm-push-4 content-wrapper">
					<div class="content-header">
						Here are some of our clients...
					</div>
					<div class="content main-content">
						<div class="news-item-wrapper">
							<ul class="clients-menu">
								<?php foreach ($logos as $key => $fields):  
									if ($fields[$logoKey]) { ?>
										<li><img src="<?php echo $fields[$logoKey]['url']; ?>"></li>
								<?php } endforeach; ?>
							</ul>
						</div>
					</div>
				</section>

				<section class="block-wrapper col-sm-pull-8 col-sm-4">

					<?php
						$defaults = array(
							'theme_location'  => 'about-us',
							'container'       => 'div',
							'container_class' => 'sidebar-menu-wrapper',
							'menu_class'      => 'side-bar-menu about-us',
							'menu_id'         => 'about_us_sidebar_menu',
						);

						wp_nav_menu( $defaults );
					?>
				</section>
			</div>
		</div>
	</article>

<?php get_footer(); // include footer.php ?>