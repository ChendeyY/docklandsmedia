<?php

/**
 * Template Name: our-team
 * @package WordPress
 * @subpackage docklands
 */

get_header(); // include header.php 

$postFields = get_fields($post->ID);
$colleagues = array();

$descriptionKey = "description";
$nameKey = "colleague-name";
$photoKey = "colleague-photo";

foreach($postFields as $key => $value) {

	$keyTemplate = $descriptionKey;
	$shards = explode($keyTemplate, $key);
	if (2 === count($shards)) {
		$colleagues[$shards[1]][$keyTemplate] = $value;
	}

	$keyTemplate = $nameKey;
	$shards = explode($keyTemplate, $key);
	if (2 === count($shards)) {
		$colleagues[$shards[1]][$keyTemplate] = $value;
	}

	$keyTemplate = $photoKey;
	$shards = explode($keyTemplate, $key);
	if (2 === count($shards)) {
		$colleagues[$shards[1]][$keyTemplate] = $value;
	}
}
?> 
	<article class="main-content">
		<div class="container">
			<div class="row main-block-wrap">
				<section class="col-sm-8 col-sm-push-4 content-wrapper">
					<div class="content-header">
						MEET OUR TEAM...
					</div>
					<div class="content main-content">
						<div class="news-item-wrapper">
						

							<?php foreach($colleagues as $key => $fields):  
								if ($fields[$photoKey] || $fields[$descriptionKey] && $fields[$descriptionKey]!='1') { ?>
									<div class="teameter-wrap">
										<div class="employee-photo">
											<img src="<?php echo $fields[$photoKey]['url']; ?>">
											<div class="teameter-name">
												<span class="our-team-name"><?php echo $fields[$nameKey]; ?></span>
											</div>
										</div>
										<p class="teameter-description">
											<?php echo $fields[$descriptionKey]; ?>
										</p>
									</div>

							<?php } endforeach; ?>

						</div>
					</div>
				</section>

				<section class="block-wrapper col-sm-pull-8 col-sm-4">
					<?php
						$defaults = array(
							'theme_location'  => 'about-us',
							'container'       => 'div',
							'container_class' => 'sidebar-menu-wrapper',
							'menu_class'      => 'side-bar-menu about-us',
							'menu_id'         => 'about_us_sidebar_menu',
						);

						wp_nav_menu( $defaults );
					?>
				</section>
			</div>
		</div>
	</article>

<?php get_footer(); // include footer.php ?>