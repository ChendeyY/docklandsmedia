(function($){
	var calendarIcon = '';

	var addRow = function()
	{
		var $table = $(this).parents('.dates-table'),
			$lastTr = $table.find('tbody tr').last();
			id = parseInt($lastTr.find('.col-1').text())+1,
			tableId = $table.parent().find('.save-button').data('id');
		if (isNaN(id)) {
			id = 1;
		}

		var row = '<tr>\
			<td class="col-1">' + id + '</td>\
			<td class="col-2">\
				<span class="input-dashed hidden"></span>\
				<input class="edit-text shown"></input>\
				<span class="dashicons dashicons-yes shown"></span>\
			</td>\
			<td class="col-3"><input type="text" class="datepicker" id="datepicker-' + id + '-start-' + tableId + '"></td>\
			<td class="col-4"><input type="text" class="datepicker" id="datepicker-' + id + '-end-' + tableId + '"></td>\
			<td class="col-5">\
				<span class="dashicons dashicons-trash"></span>\
				<span class="dashicons dashicons-admin-page"></span>\
			</td>\
		</tr>';

		if ($lastTr.length > 0) {
			$lastTr.after(row);
		} else {
			$table.find('tbody').html(row);
		}
		setTimeout(reInitDatepicker, 100);
	};

	var save = function()
	{
		var table = [],
			validation = false,
			msg = '',
			id = $(this).data('id'),
			title = $(this).parents('.inside').find('.title-block input').val();

		$('.dates-table-'+id + ' tbody').find('tr').each(function(index, el) {
			var obj = {};
			obj.id = $(this).find('.col-1').text();
			$name = $(this).find('.col-2 .input-dashed');
			obj.name = $name.text();
			obj.start = $(this).find('.col-3 .datepicker').val();
			if (obj.start == '') {
				$(this).find('.col-3 .datepicker').addClass('error');
				validation = true;
				msg = 'Please, fill start date field.';
				return false;
			}
			obj.end = $(this).find('.col-4 .datepicker').val();
			if (!isValidDate(obj.start)) {
				validation = true;
				$(this).find('.col-3 .datepicker').addClass('error');
				msg = 'Please, fill start date field with correct date.';
				return false;
			}
			table.push(obj);
		});
		
		if (validation) {
			var $error = $('.dates-table-'+id).parent().find('.error-msg');
			$error.text(msg);
			$error.css('visibility', 'visible');
			setTimeout(function() {
				$error.css('visibility', 'hidden');
				$error.text('');
			}, 2000);
			return;
		}

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			dataType: 'json',
			data: {action: 'save_events', table: table, id: id, title: title},
		})
		.done(function(answer) {
			if (typeof answer == 'undefined' || typeof answer.status == 'undefined' || !answer.status) {
				alert('Something was wrong.');
				return;
			}
			alert('Successfully saved!');
		});
		
	};

	var editText = function()
	{
		var text = $(this).text();
		$(this).hide();
		$(this).parent().find('.edit-text').val(text).show();
		$(this).parent().find('.dashicons-yes').show();
	};

	var saveText = function()
	{
		var $input = $(this).parent().find('.edit-text');
		var text = $input.val();
		$input.removeClass('shown');
		$(this).removeClass('shown');
		$input.removeClass('error').hide();
		$(this).hide();

		$(this).parent().find('.input-dashed').text(text).removeClass('hidden').show();
	};

	var deleteRow = function()
	{
		var $that = $(this);
		$row = $that.parent().parent();
		$row.fadeOut(400, function(){
			$row.remove();
		});
	};

	var copyRow = function()
	{
		var $copy = $(this).parent().parent().clone()
			$lastRow = $(this).parents('.dates-table').find('tbody tr').last()
			id = parseInt($lastRow.find('td').first().text())+1,
			tableId = $(this).parents('.dates-table').data('id');

		$copy.find('.col-1').text(id);

		var oldText = $copy.find('.input-dashed').text();
		
		if (oldText.indexOf(' (copy)') === -1) {
			$copy.find('.input-dashed').text(oldText + ' (copy)');
		}
		$copy.find('.col-3 .datepicker').attr('id', 'datepicker-' + id + '-start-' + tableId).removeClass('hasDatepicker');
		$copy.last().find('.col-4 .datepicker').attr('id', 'datepicker-' + id + '-end-' + tableId).removeClass('hasDatepicker');
		$copy.find('.ui-datepicker-trigger').remove();
		$lastRow.after($copy[0]);
		setTimeout(reInitDatepicker, 50);
	};

	var isValidDate = function(date)
	{
	    var matches = /^(\d{2})[/\-](\d{2})[/\-](\d{4})$/.exec(date);
	    if (matches == null) return false;
	    var d = matches[1];
	    var m = matches[2] - 1;
	    var y = matches[3];
	    var composedDate = new Date(y, m, d);
	    return composedDate.getDate() == d &&
	            composedDate.getMonth() == m &&
	            composedDate.getFullYear() == y;
	}

	var reInitDatepicker = function()
	{
		$('.datepicker').datepicker({
			showOtherMonths: true,
			selectOtherMonths: true,
			showOn: "button",
			buttonImage: calendarIcon,
			buttonImageOnly: true,
			buttonText: "Select date",
			dateFormat: 'dd-mm-yy',
			altFormat: 'dd-mm-yy',
			onSelect: function() {
			    $(this).removeClass('error');
			}
		});
	};

	var toggleEvent = function() {
		$(this).parent().find('.event-body').toggle(400);
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
		} else {
			$(this).addClass('active');
		}
	};
	var init = function()
	{
		calendarIcon = params.templateUrl + '/images/icons/calendar.png';
		reInitDatepicker();
		$(document).on('click', '.add-row-date', addRow);
		$(document).on('click', '.save-button', save);
		$(document).on('click', '.dates-table .dashicons-trash', deleteRow);
		$(document).on('click', '.dates-table .input-dashed', editText);
		$(document).on('click', '.dates-table .dashicons-yes', saveText);
		$(document).on('click', '.dates-table .dashicons-admin-page', copyRow);
		$(document).on('click', '.event-name', toggleEvent);
	};

	$(document).ready(init);
})(jQuery);